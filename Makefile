DOCNAME=statement
SRC=$(DOCNAME).tex $(DOCNAME).bib

all: $(DOCNAME).pdf

.PHONY: cont clean view

$(DOCNAME).pdf: $(SRC)
	latexmk -pdf $<

cont: $(SRC)
	ls $^ | entr make $(DOCNAME).pdf

view: $(DOCNAME).pdf
	xdg-open $< &

clean:
	latexmk -C
	-rm *.bbl *Notes.bib *-blx.bib *.xml
